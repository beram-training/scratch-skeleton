# Contributing Guide

## Install

```bash
make install
```

## Quality check

```bash
make quality-check
```
